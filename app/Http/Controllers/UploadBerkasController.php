<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as Xlsximport;
use ZipArchive;


class UploadBerkasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  function index()
    {
        return view('berkas.index', [
            'title' => 'Upload Berkas File System'
        ]);
    }
    public function uploadBerkas(Request $request)
    {
        try {
            //code...

            $berkasPasien = ($request->file('berkas_pasien'));
            $berkasPerbaikan = ($request->file('berkas_perbaikan'));

            $extension = $berkasPasien->getClientOriginalExtension();
            if ('csv' == $extension) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
                $reader = new Xlsximport();
            }

            $spreadsheet = $reader->load($berkasPasien->getPathname());
            $sheetData = $spreadsheet->getActiveSheet()->toArray();


            $namaBerkasPasien = $request->input('nama_file') . '-' . time();
            $folderCreate = 'berkas/' . $namaBerkasPasien;
            $fileNameZip = $namaBerkasPasien . '.zip';
            $zip = new ZipArchive;

            if (true === ($zip->open(public_path('berkas/' . $fileNameZip), ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
                for ($i = 1; $i < count($sheetData); $i++) {
                    if ($sheetData[$i][0]) {
                        if (isset($sheetData[$i][0])) {
                            $noRm = ($sheetData[$i][0]);
                            if ($noRm != null) {
                                foreach ($berkasPerbaikan as $key => $resultPerbaikan) {
                                    // data berkas perbaikan
                                    $namaBerkasPerbaikan = $resultPerbaikan->getClientOriginalName();
                                    $extensionBerkasPerbaikan = $resultPerbaikan->getClientOriginalExtension();
                                    $explodeBerkasPerbaikan = explode('.', $namaBerkasPerbaikan);

                                    // nama berkas pasien
                                    $namaBerkasPasien = $sheetData[$i][4];
                                    $fixNamaBerkasPerbaikan = $explodeBerkasPerbaikan[0] . '.' . $extensionBerkasPerbaikan;

                                    // check nama file yang sama
                                    if ($namaBerkasPasien == $fixNamaBerkasPerbaikan) {
                                        $noRmBerkasPasien = $sheetData[$i][0];

                                        $digitNumber = sprintf("%06s", $noRmBerkasPasien);

                                        $digit1 = substr($digitNumber, 0, 2);
                                        $digit2 = substr($digitNumber, 2, 2);
                                        $digit3 = substr($digitNumber, 4, 2);

                                        $noRekamMedis = $digit1 . '-' . $digit2 . '-' . $digit3;
                                        $jenisBerkas = ucwords($sheetData[$i][6]);

                                        $path = public_path() . '/' . $folderCreate . '/' . $noRekamMedis . '/' . $jenisBerkas;
                                        $folderRoot = public_path() . '/' . $folderCreate;

                                        // create folder root
                                        if (!file_exists($folderRoot)) {
                                            // File::makeDirectory($folderRoot, 0777, true, true);
                                        }

                                        // crate folder rekam medis
                                        $pathRekamMedis = public_path() . '/' . $folderCreate . '/' . $noRekamMedis;
                                        if (!file_exists($pathRekamMedis)) {
                                            // File::makeDirectory($pathRekamMedis, 0777, true, true);
                                        }

                                        // create folder jenis berkas
                                        $pathJenisBerkas = public_path() . '/' . $folderCreate . '/' . $noRekamMedis . '/' . $jenisBerkas;
                                        if (!file_exists($pathJenisBerkas)) {
                                            // File::makeDirectory($pathJenisBerkas, 0777, true, true);
                                        }

                                        $pathJenisBerkasZip =  $fileNameZip . '/' . $noRekamMedis . '/' . $jenisBerkas;
                                        $zip->addFile($resultPerbaikan->getRealPath(), $pathJenisBerkasZip . '/' . $fixNamaBerkasPerbaikan);
                                        // $this->uploadFile($resultPerbaikan, $fixNamaBerkasPerbaikan, $pathJenisBerkas);
                                    }
                                }
                            }
                        }
                    }
                }
                $zip->close();
            }

            return response()->download(public_path('berkas/' . $fileNameZip));
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Terjadi kesalahan data',
                'result' => $e->getMessage()
            ], 400);
        }
    }
}
