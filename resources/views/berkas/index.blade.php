<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container m-3">
        <div class="row">
            <div class="col-lg-12">
                <h3>Upload File Berkas Pasien</h3>
                <span class="text-dark">Silahkan teman-teman download file <strong class="text-dark">Berkas
                        Pasien</strong>
                    terlebih dahulu kemudian Upload pada form dibawah ini:
                </span>
                <a href="{{ asset('public/filePasien/berkas-pasien.xlsx') }}" class="btn btn-success float-end"
                    target="_blank">
                    DOWNLOAD BERKAS</a>
                <div class="mb-4"></div>
                <form action="{{ route('berkas.uploadBerkas') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td>Nama File</td>
                                <td>:</td>
                                <td><input type="text" class="form-control" name="nama_file" required
                                        placeholder="Nama file..."></td>
                            </tr>
                            <tr>
                                <td>Berkas Pasien (*xlsx)</td>
                                <td>:</td>
                                <td><input type="file" class="berkas_pasien form-control" name="berkas_pasien"
                                        required
                                        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                </td>
                            </tr>
                            <tr>
                                <td>Berkas Perbaikan (*pdf)</td>
                                <td>:</td>
                                <td><input type="file" class="berkas_perbaikan form-control"
                                        name="berkas_perbaikan[]" multiple required accept="application/pdf">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><button type="submit" class="btn btn-primary">SUBMIT</button></td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js"></script>
</body>

</html>
