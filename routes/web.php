<?php

use App\Http\Controllers\UploadBerkasController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear', function () {
    Artisan::call('route:cache');
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
});

Route::get('/', [UploadBerkasController::class, 'index'])->name('berkas.index');
Route::post('/upload-berkas', [UploadBerkasController::class, 'uploadBerkas'])->name('berkas.uploadBerkas');
